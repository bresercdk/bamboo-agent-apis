package it.com.edwardawebb.bamboo.agentapis.rest;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.ClientResponse;
import org.junit.After;
import org.junit.Test;

import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

public class AgentStateEndpointTest extends AbstractEndpointTest{

    final static String AGENT_ID="131073";
    
    String resourceUrlAgentState = agentBaseUrl + "%s/state";
    
    
    @After
    public void tearDown() {

    }
    
   

    @Test
    public void agentStatusCanBeRetrievedWithValidToken() {
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readOnlyToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readOnlyToken = response.getEntity(TokenResource.class);
         response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID) +"?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).get();
         assertEquals("Could not retrive status",200,response.getStatusCode());
         AgentStateModel agentState = response.getEntity(AgentStateModel.class);
        assertEquals("Retrieved invalid agent ID",Long.parseLong(AGENT_ID),agentState.getId());
    }

    @Test
    public void anAgentCanNotBeDisabledWithReadOnlyToken(){ 
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readOnlyToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readOnlyToken = response.getEntity(TokenResource.class);
         response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/disable?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Was allowed to change with read only token",403,response.getStatusCode());
     }
    
    @Test
    public void anAgentCanBeDisabledWithWriteToken(){ 
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        
        
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
         response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/disable?uuid=" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Could not disable agent",200,response.getStatusCode());
    }

    @Test
    public void anAgentCanNotBeEnabledWithReadOnlyToken(){ 
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readOnlyToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readOnlyToken = response.getEntity(TokenResource.class);
         response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/enable?uuid=" + readOnlyToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Was allowed to change with read only token",403,response.getStatusCode());
     }
    
    @Test
    public void anAgentCanBeEnabledWithWriteToken(){ 
        ClientResponse response = client.resource(resourceUrlToken).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(readAndWriteToken);
        assertEquals("Could not create token needed for test",200,response.getStatusCode());
        readAndWriteToken = response.getEntity(TokenResource.class);
         response = client.resource(String.format(resourceUrlAgentState,  AGENT_ID)+ "/enable?uuid=" + readAndWriteToken.getUuid()).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(true);
        assertEquals("Could not enable agent",200,response.getStatusCode());
    }
    
    
    
    
    
    
}
