package com.edwardawebb.bamboo.agentapis.ao.model;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.Preload;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;

@Table("TOKENS")
@Preload
/**
 * This tracks what level of access each token is granted.
 *
 */
public interface AccessToken extends Entity {

    @Unique
    @Mutator("UUID")
    public void setUuid(String config);
    @Accessor("UUID")
    public String getUuid();
    
  
    @Mutator("NAME")
    public void setName(String name);
    @Accessor("NAME")
    public String getName();

    @Mutator("READ")
    public boolean isAllowedToRead();
    @Accessor("READ")
    public void setAllowedToRead(boolean canRead);

    @Mutator("CHANGE")
    public boolean isAllowedToChange();
    @Accessor("CHANGE")
    public void setAllowedToChange(boolean canChange);

   
}
