

(function ($) { // this closure helps us keep our variables to ourselves.
// This pattern is known as an "iife" - immediately invoked function expression


    // form the URL
    var url = AJS.contextPath() + "/rest/agent-config/1.0/tokens";

    
    function loadTokens() {
    	  // request the config information from the server
        AJS.$.ajax({
            url: url,
            dataType: "json"
	        }).done(function(tokens) { // when the configuration is returned...
	            // ...populate the form.
	        	console.log(tokens);
	        	var tokens = Bamboo.Templates.AgentAPITokens.tokenRows({tokens:tokens});  
	 	          AJS.$("#tokenRows").html(tokens); 
	 	         
	        });
    }

	function createToken(event) {
		console.log(event);
		var data = '{ "name": "' + AJS.$("#newTokenName").attr("value") + '", "read": "' +  AJS.$("#newTokenCanRead").prop("checked") + '", "change": "' +  AJS.$("#newTokenCanChange").prop("checked") + '" }';
		console.log(data);
		  AJS.$.ajax({
		    url: url,
		    type: "POST",
		    contentType: "application/json",
		    data: data,
		    processData: false,
		    success: function(data) {
		         AJS.messages.success({
		            title: "Saved!",
		            body: "Token created!"
		         }); 
		         loadTokens();
		     }	
		    
		  });
	}

	function deleteToken(event) {
		console.log(event);
		var muuid = AJS.$(event.srcElement).data("uuid");
		var mname = AJS.$(event.srcElement).data("name");
		console.log("deleting token with UUID " + muuid);
		AJS.$.ajax({
			    url: url + "/" + muuid,
			    type: "DELETE",
			    contentType: "application/json",
			    processData: false,
			    success: function(data) {
			         AJS.messages.success({
			            title: "Deleted!",
			            body: "Token '" + mname + "' was successfully deleted."
			         }); 
			         loadTokens();
			     }	
			    
		 });
		
	}
    
	function updateToken() {
		var muuid = AJS.$(event.srcElement).data("uuid");
		var mname = AJS.$(event.srcElement).data("name");
		var mread = AJS.$("#"+muuid+"read").prop("checked");
		var mchange = AJS.$("#"+muuid+"change").prop("checked");
		var data = '{"name":"'+mname+'","uuid":"'+muuid+'","read":'+mread+',"change":'+mchange+'}';
		console.log("updating token with UUID " + muuid);
		console.log(data);
		AJS.$.ajax({
			    url: url + "/" + muuid,
			    type: "PUT",
			    data: data,
			    contentType: "application/json",
			    processData: false,
			    success: function(data) {
			         AJS.messages.success({
			            title: "Updated!",
			            body: "Token '" + mname + "' was successfully updated."
			         }); 
			         loadTokens();
			     }	
			    
		 });
	}
    
    // wait for the DOM (i.e., document "skeleton") to load. This likely isn't
	// necessary for the current case,
    // but may be helpful for AJAX that provides secondary content.
    $(document).ready(function() {
    	//bind our buttons, etc
    	AJS.$("#agentApiTokens").submit(function(e) {
    		console.log("SUBMITING!");
    	    e.preventDefault();
    	    createToken(e);
    	});
    	AJS.$(document).on('click', "a.deleteToken", function(e) {
    	    e.preventDefault();
    		deleteToken(e);
    		return false;
    	});
    	AJS.$(document).on('change', "input.tokenPermissionCheckbox", function(e) {
    	    e.preventDefault();
    		updateToken(e);
    		return false;
    	});
    	// load data
        loadTokens();
        //show help
    	AJS.InlineDialog(AJS.$("#popupLink"), 1,
    	    function(content, trigger, showPopup) {
    	        content.css({"padding":"20px"}).html($('#popupHelpBody').html() );
    	        showPopup();
    	        return false;
    	    }
    	);
    	
    });

})(AJS.$ || jQuery);












