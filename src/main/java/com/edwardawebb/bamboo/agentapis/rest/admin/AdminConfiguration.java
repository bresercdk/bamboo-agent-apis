package com.edwardawebb.bamboo.agentapis.rest.admin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.edwardawebb.bamboo.agentapis.enums.UpgradeStance;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AdminConfiguration {
    @XmlElement
    private UpgradeStance stance;
    @XmlElement
    private int maxactive;
    @XmlElement
    private String targetVersion;
    
    
    public UpgradeStance getStance() {
        return stance;
    }
    public void setStance(UpgradeStance stance) {
        this.stance = stance;
    }
    public int getMaxactive() {
        return maxactive;
    }
    public void setMaxactive(int maxactive) {
        this.maxactive = maxactive;
    }
    public String getTargetVersion() {
        return targetVersion;
    }
    public void setTargetVersion(String targetVersion) {
        this.targetVersion = targetVersion;
    }

    
    public static AdminConfiguration getActiveConfig(PluginSettingsFactory pluginSettingsFactory){
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        AdminConfiguration config = new AdminConfiguration();
        try{
            config.setStance( UpgradeStance.valueOf((String)settings.get(AdminConfiguration.class.getName() + ".stance")));
        }catch(NullPointerException npe){
            //first time, select list will default.
            config.setStance( UpgradeStance.STABLE);
        }
        String targetVersion = (String) settings.get(AdminConfiguration.class.getName() + ".targetVersion");
        if (targetVersion != null) {
            config.setTargetVersion(targetVersion);
        }
        
        String maxActive = (String) settings.get(AdminConfiguration.class.getName() + ".maxactive");
        if (maxActive != null) {
            config.setMaxactive(Integer.parseInt(maxActive));
        }
        
        
        return config;
    }
}
