<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.edwardawebb</groupId>
	<artifactId>bamboo-agent-apis</artifactId>
	<version>2.1.2-SNAPSHOT</version>
	<organization>
		<name>Edward A. Webb</name>
		<url>http://www.edwardawebb.com/</url>
	</organization>
	<name>bamboo-agent-apis</name>
	<description>Provide the suspiciously absent API for agent management (enable/disable, current state, etc). 
    Enhancement suggestions and contributions are highly encouraged (and requested!)</description>
	<packaging>atlassian-plugin</packaging>


	<scm>
		<connection>scm:git:ssh://git@bitbucket.org/eddiewebb/bamboo-agent-apis.git</connection>
	</scm>

	<properties>
		<bamboo.version>5.9.0</bamboo.version>
		<bamboo.data.version>5.3</bamboo.data.version>
		<amps.version>4.2.20</amps.version>
		<plugin.testrunner.version>1.1.4</plugin.testrunner.version>
		<ao.version>0.25.2</ao.version>

	</properties>

	<distributionManagement>
		<repository>
			<uniqueVersion>false</uniqueVersion>
			<id>eawReleases</id>
			<name>eawReleases</name>
			<url>scp://eawmvn@maven.edwardawebb.com/home/eawmvn/webroot/repository/releases</url>
			<layout>default</layout>
		</repository>
		<snapshotRepository>
			<uniqueVersion>true</uniqueVersion>
			<id>eawSnapshots</id>
			<url>scp://eawmvn@maven.edwardawebb.com/home/eawmvn/webroot/repository/snapshots</url>
			<name>eawSnapshots</name>
		</snapshotRepository>
	</distributionManagement>



	<dependencies>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-web</artifactId>
			<version>${bamboo.version}</version>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>javax.jms</groupId>
					<artifactId>jms</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>com.atlassian.activeobjects</groupId>
			<artifactId>activeobjects-plugin</artifactId>
			<version>${ao.version}</version>
			<scope>provided</scope>
		</dependency>
		<!-- SAL, the Active Objects plugin uses SAL's API for transactions -->
		<dependency>
			<groupId>com.atlassian.sal</groupId>
			<artifactId>sal-api</artifactId>
			<version>2.4.1</version>
			<scope>provided</scope>
		</dependency>

<dependency>
  <groupId>com.atlassian.templaterenderer</groupId>
  <artifactId>atlassian-template-renderer-api</artifactId>
  <version>1.1.1</version>
  <scope>provided</scope>
</dependency>

		<!-- TEST DEPENDENCIES -->
		<dependency>
			<groupId>org.codehaus.jackson</groupId>
			<artifactId>jackson-core-asl</artifactId>
			<version>1.9.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.codehaus.jackson</groupId>
			<artifactId>jackson-jaxrs</artifactId>
			<version>1.9.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.codehaus.jackson</groupId>
			<artifactId>jackson-mapper-asl</artifactId>
			<version>1.9.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.codehaus.jackson</groupId>
			<artifactId>jackson-xc</artifactId>
			<version>1.9.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.10</version>
			<scope>test</scope>
		</dependency>
		<!-- WIRED TEST RUNNER DEPENDENCIES -->
		<dependency>
			<groupId>com.atlassian.plugins</groupId>
			<artifactId>atlassian-plugins-osgi-testrunner</artifactId>
			<version>${plugin.testrunner.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>javax.ws.rs</groupId>
			<artifactId>jsr311-api</artifactId>
			<version>1.1.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.2.2-atlassian-1</version>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.4</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.plugins.rest</groupId>
			<artifactId>atlassian-rest-common</artifactId>
			<version>1.0.2</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.sal</groupId>
			<artifactId>sal-api</artifactId>
			<version>2.6.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.wink</groupId>
			<artifactId>wink-client</artifactId>
			<version>1.1.3-incubating</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.8.5</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
	<build>
		<plugins>

			<plugin>
				<groupId>com.atlassian.maven.plugins</groupId>
				<artifactId>maven-bamboo-plugin</artifactId>
				<version>${amps.version}</version>
				<extensions>true</extensions>
				<configuration>
					<productVersion>${bamboo.version}</productVersion>
					<productDataVersion>${bamboo.data.version}</productDataVersion>
					<productDataPath>${project.basedir}/src/test/resources/generated-test-resources.zip</productDataPath>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
		</plugins>
		<extensions>
			<extension>
				<groupId>org.apache.maven.wagon</groupId>
				<artifactId>wagon-ssh</artifactId>
				<version>2.4</version>
			</extension>
		</extensions>
	</build>
</project>
