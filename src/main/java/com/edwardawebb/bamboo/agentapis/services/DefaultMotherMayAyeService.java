package com.edwardawebb.bamboo.agentapis.services;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.edwardawebb.bamboo.agentapis.ao.AgentInProgressService;
import com.edwardawebb.bamboo.agentapis.ao.model.AgentInProgress;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.enums.PermissionEnum;
import com.edwardawebb.bamboo.agentapis.rest.admin.AdminConfiguration;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.AlreadyInProgressException;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.UpgradePermissionModel;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

public class DefaultMotherMayAyeService implements MotherMayAyeService {
    private final AgentService remoteAgentService;
    private  final AgentInProgressService agentInProgressService;
    private final PluginSettingsFactory pluginSettingsFactory;
    private static final Logger log = LoggerFactory
            .getLogger(DefaultMotherMayAyeService.class);

    
    
    
    public DefaultMotherMayAyeService(AgentService remoteAgentService, AgentInProgressService agentInProgressService,PluginSettingsFactory pluginSettingsFactory) {
        this.remoteAgentService = remoteAgentService;
        this.agentInProgressService = agentInProgressService;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }
    
   

    @Override
    public UpgradePermissionModel markInProgressIfAllowed(AgentActivity upgrade, long agentId, UUID uuid) {
        log.warn("Agent {} requesting update",agentId);
        AgentStateModel agentState = remoteAgentService.getStateFor(agentId);
        BuildAgent agent = remoteAgentService.getAgentManager().getAgent(agentId);
        AdminConfiguration config = AdminConfiguration.getActiveConfig(pluginSettingsFactory);
        
        
        
        if(config.getStance().isAllowed()){
            List<AgentInProgress> currentActivity = agentInProgressService.existingActivityForAllAgents();
            log.debug("found {} other updates in progress",currentActivity.size());
            if(currentActivity.size() < config.getMaxactive()){    
                log.info("allowing {} to upgrade. They willbe disabled. The agent should respect any running jobs before it begins work.",agentId);
                try{
                    AgentInProgress task = agentInProgressService.markAgentInProgress(agent, AgentActivity.UPGRADE, uuid, agentState);
                    agentState = remoteAgentService.disable(agentId);              
                    return new UpgradePermissionModel(PermissionEnum.YES_CHILD,"you may upgrade once idle",agentState,config.getTargetVersion(),task.getID());                    
                }catch(AlreadyInProgressException aip){
                    return new UpgradePermissionModel(PermissionEnum.UH_OH,PermissionEnum.UH_OH.getMessage(),agentState,config.getTargetVersion(),aip.getTaskId());                    
                }
            }else{
                log.info("asking agent {} to wait since the max # is already reached",agentId);
                
                return new UpgradePermissionModel(PermissionEnum.WAIT_FOR_SIBLINGS,currentActivity.size() + " agents alreaddy updating, please check back soon.",agentState);
            }
        }else{
            log.info("Locked in stable state, no updates allowed. SOrry agent {}",agentId);
            
            return new UpgradePermissionModel(PermissionEnum.NO_CHILD,config.getStance().getDescription(),agentState);
        }
    }



    @Override
    public UpgradePermissionModel markComplete( int taskId) {
        AgentInProgress taskDetails = agentInProgressService.markTaskComplete(taskId);
        AgentStateModel agentState = remoteAgentService.getStateFor(taskDetails.getAgentId());
        if(taskDetails.isAgentActive()){
            agentState = remoteAgentService.enable(taskDetails.getAgentId());
            log.warn("Agent was active before maintenance, and is attempted to be re-activated");
        }else{

            log.warn("Agent was disabled before requesting maintenance, so it will not be activated now");
        }
        return new UpgradePermissionModel(PermissionEnum.NO_CHILD,"Thanks for updating, letting the others catch up.",agentState);
        
    }
    
    
}
