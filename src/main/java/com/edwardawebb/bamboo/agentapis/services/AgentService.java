package com.edwardawebb.bamboo.agentapis.services;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

public interface AgentService {

    AgentStateModel getStateFor(long key);

    AgentStateModel disable(long key);

    AgentStateModel enable(long key);

    ReadOnlyCapabilitySet listCapabilities(long agentId);
    void deleteAllCapabilities(long agentId);

    AgentManager getAgentManager();
}