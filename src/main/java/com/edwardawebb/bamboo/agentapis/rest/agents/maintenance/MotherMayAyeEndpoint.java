package com.edwardawebb.bamboo.agentapis.rest.agents.maintenance;

import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.services.MotherMayAyeService;

/**
 * A resource of message.
 */
@Path("{id}/maintenance")
@PublicApi
public class MotherMayAyeEndpoint {
    
    private MotherMayAyeService motherMayAyeService;
    
    private AccessTokenService accessTokenService;

    
    
    

    public MotherMayAyeEndpoint(MotherMayAyeService motherMayAyeService, AccessTokenService accessTokenService) {
        this.motherMayAyeService = motherMayAyeService;
        this.accessTokenService = accessTokenService;
    }

    @GET
    public Response lostUserInfo()
    {
       return Response.ok("Please see user docs for use.").build();
    }


    /**
     * Agents may call requesting permission to do any action available in @AgentActivity
     * If this controller detects the agent will be allowed, it requests the agent be disabled as well.
     * Agents must themselves confirm their status of idle and disabled before upgrading.
     * 
     * Once upgraded agents must use the @AgentState endpoint to re-enable.
     * @param id
     * @param uuid
     * @return
     */
    @POST
    @AnonymousAllowed
    public Response updgrade(@PathParam("id") long agentId,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return   Response.status(Status.BAD_REQUEST).entity("No UUID Supplied").build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);
       
       if(null != token && token.isAllowedToChange()){           
           UpgradePermissionModel response = motherMayAyeService.markInProgressIfAllowed(AgentActivity.UPGRADE,agentId,uuid);           
           return Response.ok(response.asText()).build();
       }else{
           return Response.status(Status.FORBIDDEN).build();
       }
        
    }
    
    @PUT
    @AnonymousAllowed
    @Path("{taskId}/finish")
    public Response finish(@PathParam("taskId") int taskId,@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return   Response.status(Status.BAD_REQUEST).entity("No UUID Supplied").build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);
       
       if(token.isAllowedToChange()){
           
           UpgradePermissionModel response = motherMayAyeService.markComplete(taskId);
           
           return Response.ok(response.asText()).build();
       }else{
           return Response.status(Status.FORBIDDEN).build();
       }
        
    }
}