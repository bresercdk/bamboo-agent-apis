package com.edwardawebb.bamboo.agentapis.rest.admin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
@XmlRootElement(name = "token")
@XmlAccessorType(XmlAccessType.FIELD)
public class TokenResource {
    @XmlElement(name = "uuid")
    private String uuid;
    @XmlElement(name = "read")
    private boolean isAllowedRead;
    @XmlElement(name = "change")
    private boolean isAllowedChange;
    @XmlElement(name = "name")
    private String name;
    

    public TokenResource() {   
    }
    
    public TokenResource(String name, boolean isAllowedRead, boolean isAllowedChange) {
        this.isAllowedRead = isAllowedRead;
        this.isAllowedChange = isAllowedChange;
        this.name = name;
    }

    
    public TokenResource(String uuid,String name, boolean isAllowedRead, boolean isAllowedChange) {
        this.isAllowedRead = isAllowedRead;
        this.isAllowedChange = isAllowedChange;
        this.name = name;
        this.uuid = uuid;
    }

    public static TokenResource from(AccessToken token) {
        // TODO Auto-generated method stub
        TokenResource resource = new TokenResource();
        resource.uuid=token.getUuid();
        resource.isAllowedChange = token.isAllowedToChange();
        resource.isAllowedRead = token.isAllowedToRead();
        resource.name = token.getName();
        return resource;
    }
    
    




    public String getUuid() {
        return uuid;
    }
    public boolean isAllowedRead() {
        return isAllowedRead;
    }
    public boolean isAllowedChange() {
        return isAllowedChange;
    }
    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
    
    
    
    
}
