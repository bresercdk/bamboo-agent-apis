package com.edwardawebb.bamboo.agentapis.ao.model;

import java.util.Date;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;

@Table("ACTIVITY")
@Preload
/**
 * This tracks an agent who requested to do something, and has not completed.
 *
 */
public interface AgentInProgress extends Entity {

   
    @Mutator("UUID")
    public void setUuid(String uuid);
    @Accessor("UUID")
    public String getUuid();
    

    @Mutator("NAME")
    public void setName(String name);
    @Accessor("NAME")
    public String getName();
    

    @Indexed
    @Mutator("AID")
    public void setAgentId(long id);

    @Indexed
    @Accessor("AID")
    public long getAgentId();
    
    
    @Mutator("ACTION")
    public void setActivity(AgentActivity name);
    @Accessor("ACTION")
    public AgentActivity getActivity();

    @Indexed
    @Mutator("ACTIVE")
    public void setInProgress(boolean isInProgress);
    @Accessor("ACTIVE")
    @Indexed
    public boolean isInProgress();   
    
    @Mutator("AGNTSTAT")
    public void setAgentActive(boolean currentState);
    @Accessor("AGNTSTAT")
    public boolean isAgentActive();

    @Mutator("START_DATE")
    public void setStartDate(Date startDate);
    @Accessor("START_DATE")
    public Date getSTartDate();


    @Mutator("END_DATE")
    public void setEndDate(Date startDate);
    @Accessor("END_DATE")
    public Date getEndtDate();

   
}
