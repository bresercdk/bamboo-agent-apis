package com.edwardawebb.bamboo.agentapis.services.audit;

import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.ao.model.AgentInProgress;
import com.edwardawebb.bamboo.agentapis.rest.admin.TokenResource;

public interface AgentApiAuditService {

    void createTokenEvent(AccessToken token);

    void deleteTokenEvent(AccessToken token);

    void updateTokenEvent(AccessToken token, TokenResource resource);

    void startAcvityEvent(AgentInProgress record);

}