package com.edwardawebb.bamboo.agentapis.services;

import java.util.UUID;

import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.UpgradePermissionModel;

public interface MotherMayAyeService {

    UpgradePermissionModel markInProgressIfAllowed(AgentActivity upgrade, long agentId,UUID uuid);

    UpgradePermissionModel markComplete( int taskId);

}
